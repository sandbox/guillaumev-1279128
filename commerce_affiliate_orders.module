<?php

/**
 * @file Commerce Affiliate Orders
 */

/**
 * Implements hook_entity_info().
 */
function commerce_affiliate_orders_entity_info() {
  $return = array(
    'commerce_affiliate_order' => array(
      'label' => t('Commerce Affiliate order'),
      'entity class' => 'EntityCommerceAffiliateOrder',
      'controller class' => 'EntityAPIController',
      'views controller class' => 'EntityDefaultViewsController',
      // Don't let Entity API auto-generate the property info.
      'metadata controller class' => '',
      'module' => 'commerce_affiliate_orders',
      'base table' => 'commerce_affiliate_order',
      'fieldable' => TRUE,
      'entity keys' => array(
        'id' => 'cao_id',
      ),
    ),
  );

  return $return;
}

/**
 * Implements hook_commerce_checkout_complete()
 */
function commerce_affiliate_orders_commerce_checkout_complete($order) {
  // Check whether this order comes from an affiliate site
  if (isset($_COOKIE['affiliate_ng']['affiliate_uid']) && isset($_COOKIE['affiliate_ng']['campaign_id'])) {
    $campaign_id = $_COOKIE['affiliate_ng']['campaign_id'];
    $affiliate_uid = $_COOKIE['affiliate_ng']['affiliate_uid'];
    // Register the order.
    $values = array(
      'campaign_id' => $campaign_id,
      'affiliate_uid' => $affiliate_uid,
      'order_id' => $order->order_id,
      'created' => REQUEST_TIME,
      'hostname' => ip_address(),
      'referrer' => $_SERVER['HTTP_REFERER'],
    );
    $affiliate_order = entity_create('commerce_affiliate_order', $values);
    $affiliate_order->save();
    // Remove cookies
    setcookie('affiliate_ng[affiliate_uid]', '', time() - 3600, '/');
    setcookie('affiliate_ng[campaign_id]', '', time() - 3600, '/');
  }
}

/**
 * Callback for getting order properties.
 * @see commerce_affiliate_orders_entity_property_info()
 */
function commerce_affiliate_orders_get_properties($order, array $options, $name) {
  switch ($name) {
    case 'order':
      return $order->order_id;
  }
}

/**
 * Callback for setting order properties.
 */
function commerce_affiliate_orders_set_properties($click, $name, $value) {
  switch ($name) {
    case 'order':
      $order->order_id = $value;
      break;
  }
}

/**
 * Implements hook_views_api().
 */
function commerce_affiliate_orders_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'commerce_affiliate_orders') . '/includes/views',
  );
}

/**
 * Main class for the Affiliate click entity type.
 */
class EntityCommerceAffiliateOrder extends Entity {
  /**
   * The id of the campaign that this order is attached to.
   *
   * @var int
   */
  public $campaign_id = 0;
  
  /**
   * The uid of the affiliate credited for the order.
   *
   * @var int
   */
  public $affiliate_uid = 0;
  
  /**
   * The order id of the referenced order
   * 
   * @var int
   */
  public $order_id = 0;
  
  /**
   * The IP address that made the order.
   *
   * @var string
   */
  public $hostname = '';
  
  /**
   * Referrer.
   * 
   * @var string
   */
  public $referrer = '';
  
  public function __construct(array $values = array(), $entityType = NULL) {
    parent::__construct($values, 'commerce_affiliate_order');
  }
  
  public function save() {
    $this->changed = REQUEST_TIME;
    if (!empty($this->is_new) && empty($this->created)) {
      $this->created = REQUEST_TIME;
    }
    
    parent::save();
  }
}
