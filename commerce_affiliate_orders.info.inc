<?php

/**
 * @file
 * Provides metadata for the commerce_affiliate_order entity type.
 */

/**
 * Implements hook_entity_property_info().
 */
function commerce_affiliate_orders_entity_property_info() {
  $info = array();

  // Add meta-data about the commerce affiliate orders properties.
  $order_properties = &$info['commerce_affiliate_order']['properties'];

  $order_properties['cao_id'] = array(
    'label' => t('Affiliate Order ID'),
    'description' => t('The internal numeric ID of the affiliate order.'),
    'type' => 'integer',
    'schema field' => 'cao_id',
  );
  $order_properties['campaign_id'] = array(
    'label' => t('Campaign ID'),
    'type' => 'integer',
    'description' => t("The ID of the referenced campaign."),
    'setter callback' => 'entity_metadata_verbatim_set',
    'setter permission' => 'administer affiliates',
    'clear' => array('campaign'),
    'schema field' => 'campaign_id',
  );
  $order_properties['campaign'] = array(
    'label' => t('Campaign'),
    'type' => 'affiliate_campaign',
    'description' => t("The referenced campaign."),
    'getter callback' => 'affiliate_ng_click_get_properties',
    'setter callback' => 'affiliate_ng_click_set_properties',
    'setter permission' => 'administer affiliates',
    'clear' => ('campaign_id'),
  );
  $order_properties['order_id'] = array(
    'label' => t('Order ID'),
    'type' => 'integer',
    'description' => t('The ID of the reference order.'),
    'setter callback' => 'entity_metadata_verbatim_set',
    'setter permission' => 'administer affiliates',
    'clear' => array('order'),
    'schema field' => 'order_id',
  );
  $order_properties['order'] = array(
    'label' => t('Order'),
    'type' => 'commerce_order',
    'description' => t('The referenced order'),
    'getter callback' => 'commerce_affiliate_orders_get_properties',
    'setter callback' => 'commerce_affiliate_orders_set_properties',
    'setter permission' => 'administer affiliates',
    'clear' => ('order_id'),
  );
  $order_properties['hostname'] = array(
    'label' => t('Hostname'),
    'description' => t("The IP address that made the click (clicked the affiliate's link)."),
    'type' => 'text',
    'setter callback' => 'entity_metadata_verbatim_set',
    'schema field' => 'hostname',
  );
  $order_properties['referrer'] = array(
    'label' => t('Referrer'),
    'description' => t('The referring site.'),
    'type' => 'text',
    'setter callback' => 'entity_metadata_verbatim_set',
    'schema field' => 'referrer',
  );
  $order_properties['created'] = array(
    'label' => t('Date created'),
    'description' => t('The date the click was created.'),
    'type' => 'date',
    'setter callback' => 'entity_metadata_verbatim_set',
    'schema field' => 'created',
  );
  $order_properties['affiliate_uid'] = array(
    'label' => t('Affiliate UID'),
    'type' => 'integer',
    'description' => t("The UID of the referenced affiliate."),
    'setter callback' => 'entity_metadata_verbatim_set',
    'setter permission' => 'administer affiliates',
    'clear' => array('affiliate'),
    'schema field' => 'affiliate_uid',
  );
  $order_properties['affiliate'] = array(
    'label' => t('Affiliate'),
    'type' => 'user',
    'description' => t("The referenced affiliate."),
    'getter callback' => 'affiliate_ng_click_get_properties',
    'setter callback' => 'affiliate_ng_click_set_properties',
    'setter permission' => 'administer affiliates',
    'required' => TRUE,
    'clear' => array('affiliate_uid'),
  );

  return $info;
}
