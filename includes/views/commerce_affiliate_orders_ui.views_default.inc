<?php

/**
 * Implements hook_views_default_views().
 */
function commerce_affiliate_orders_ui_views_default_views() {
  $views = array();
  
  $view = new view;
  $view->name = 'affiliate_orders';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'commerce_affiliate_order';
  $view->human_name = 'Affiliate orders';
  $view->core = 7;
  $view->api_version = '3.0-alpha1';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Affiliate orders';
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Relationship: Commerce Affiliate order: Order */
  $handler->display->display_options['relationships']['order_id']['id'] = 'order_id';
  $handler->display->display_options['relationships']['order_id']['table'] = 'commerce_affiliate_order';
  $handler->display->display_options['relationships']['order_id']['field'] = 'order_id';
  $handler->display->display_options['relationships']['order_id']['required'] = 0;
  /* Field: Commerce Affiliate order: Affiliate Order ID */
  $handler->display->display_options['fields']['cao_id']['id'] = 'cao_id';
  $handler->display->display_options['fields']['cao_id']['table'] = 'commerce_affiliate_order';
  $handler->display->display_options['fields']['cao_id']['field'] = 'cao_id';
  /* Field: Commerce Affiliate order: Date created */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'commerce_affiliate_order';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['created']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['created']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['created']['alter']['external'] = 0;
  $handler->display->display_options['fields']['created']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['created']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['created']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['created']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['created']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['created']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['created']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['created']['alter']['html'] = 0;
  $handler->display->display_options['fields']['created']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['created']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['created']['hide_empty'] = 0;
  $handler->display->display_options['fields']['created']['empty_zero'] = 0;
  $handler->display->display_options['fields']['created']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['created']['date_format'] = 'short';
  /* Field: Commerce Affiliate order: Hostname */
  $handler->display->display_options['fields']['hostname']['id'] = 'hostname';
  $handler->display->display_options['fields']['hostname']['table'] = 'commerce_affiliate_order';
  $handler->display->display_options['fields']['hostname']['field'] = 'hostname';
  $handler->display->display_options['fields']['hostname']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['hostname']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['hostname']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['hostname']['alter']['external'] = 0;
  $handler->display->display_options['fields']['hostname']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['hostname']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['hostname']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['hostname']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['hostname']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['hostname']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['hostname']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['hostname']['alter']['html'] = 0;
  $handler->display->display_options['fields']['hostname']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['hostname']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['hostname']['hide_empty'] = 0;
  $handler->display->display_options['fields']['hostname']['empty_zero'] = 0;
  $handler->display->display_options['fields']['hostname']['hide_alter_empty'] = 0;
  /* Field: Commerce Affiliate order: Referrer */
  $handler->display->display_options['fields']['referrer']['id'] = 'referrer';
  $handler->display->display_options['fields']['referrer']['table'] = 'commerce_affiliate_order';
  $handler->display->display_options['fields']['referrer']['field'] = 'referrer';
  $handler->display->display_options['fields']['referrer']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['referrer']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['referrer']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['referrer']['alter']['external'] = 0;
  $handler->display->display_options['fields']['referrer']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['referrer']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['referrer']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['referrer']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['referrer']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['referrer']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['referrer']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['referrer']['alter']['html'] = 0;
  $handler->display->display_options['fields']['referrer']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['referrer']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['referrer']['hide_empty'] = 0;
  $handler->display->display_options['fields']['referrer']['empty_zero'] = 0;
  $handler->display->display_options['fields']['referrer']['hide_alter_empty'] = 0;
  /* Field: Commerce Affiliate order: Order ID */
  $handler->display->display_options['fields']['order_id']['id'] = 'order_id';
  $handler->display->display_options['fields']['order_id']['table'] = 'commerce_affiliate_order';
  $handler->display->display_options['fields']['order_id']['field'] = 'order_id';
  $handler->display->display_options['fields']['order_id']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['order_id']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['order_id']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['order_id']['alter']['external'] = 0;
  $handler->display->display_options['fields']['order_id']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['order_id']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['order_id']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['order_id']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['order_id']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['order_id']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['order_id']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['order_id']['alter']['html'] = 0;
  $handler->display->display_options['fields']['order_id']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['order_id']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['order_id']['hide_empty'] = 0;
  $handler->display->display_options['fields']['order_id']['empty_zero'] = 0;
  $handler->display->display_options['fields']['order_id']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['order_id']['format_plural'] = 0;
  /* Field: Commerce Order: Order total */
  $handler->display->display_options['fields']['commerce_order_total']['id'] = 'commerce_order_total';
  $handler->display->display_options['fields']['commerce_order_total']['table'] = 'field_data_commerce_order_total';
  $handler->display->display_options['fields']['commerce_order_total']['field'] = 'commerce_order_total';
  $handler->display->display_options['fields']['commerce_order_total']['relationship'] = 'order_id';
  $handler->display->display_options['fields']['commerce_order_total']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['commerce_order_total']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['commerce_order_total']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['commerce_order_total']['alter']['external'] = 0;
  $handler->display->display_options['fields']['commerce_order_total']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['commerce_order_total']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['commerce_order_total']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['commerce_order_total']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['commerce_order_total']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['commerce_order_total']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['commerce_order_total']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['commerce_order_total']['alter']['html'] = 0;
  $handler->display->display_options['fields']['commerce_order_total']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['commerce_order_total']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['commerce_order_total']['hide_empty'] = 0;
  $handler->display->display_options['fields']['commerce_order_total']['empty_zero'] = 0;
  $handler->display->display_options['fields']['commerce_order_total']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['commerce_order_total']['click_sort_column'] = 'amount';
  $handler->display->display_options['fields']['commerce_order_total']['type'] = 'commerce_price_formatted_amount';
  $handler->display->display_options['fields']['commerce_order_total']['settings'] = array(
    'calculation' => FALSE,
  );
  $handler->display->display_options['fields']['commerce_order_total']['field_api_classes'] = 0;
  /* Contextual filter: Commerce Affiliate order: Affiliate UID */
  $handler->display->display_options['arguments']['affiliate_uid']['id'] = 'affiliate_uid';
  $handler->display->display_options['arguments']['affiliate_uid']['table'] = 'commerce_affiliate_order';
  $handler->display->display_options['arguments']['affiliate_uid']['field'] = 'affiliate_uid';
  $handler->display->display_options['arguments']['affiliate_uid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['affiliate_uid']['default_argument_type'] = 'user';
  $handler->display->display_options['arguments']['affiliate_uid']['default_argument_options']['user'] = FALSE;
  $handler->display->display_options['arguments']['affiliate_uid']['default_argument_skip_url'] = 0;
  $handler->display->display_options['arguments']['affiliate_uid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['affiliate_uid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['affiliate_uid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['affiliate_uid']['break_phrase'] = 0;
  $handler->display->display_options['arguments']['affiliate_uid']['not'] = 0;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'user/%/affiliate/orders';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Affiliate orders';
  $handler->display->display_options['menu']['weight'] = '0';
  $translatables['affiliate_orders'] = array(
    t('Master'),
    t('Affiliate orders'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('Order'),
    t('Affiliate Order ID'),
    t('.'),
    t(','),
    t('Date created'),
    t('Hostname'),
    t('Referrer'),
    t('Order ID'),
    t('Order total'),
    t('All'),
    t('Page'),
  );


  $views[$view->name] = $view;
  return $views;
}
