<?php
// $Id$

/**
 * Export the affiliate orders to Views.
 */

/**
 * Implements hook_views_data_alter()
 */
function commerce_affiliate_orders_views_data_alter(&$data) {
  $data['commerce_affiliate_order']['order_id']['relationship'] = array(
    'title' => t('Order'),
    'help' => t('Relate this affiliate order to the order'),
    'handler' => 'views_handler_relationship',
    'base' => 'commerce_order',
    'base field' => 'order_id',
    'field' => 'order_id',
    'label' => t('Order')
  );
}

